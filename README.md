# OpenFlow-based Network Admission Control system

## About
The purpose of the system is to ensure all devices connected are trusted and eligible for network access.

## Topology
The exhibit topology used to demonstrate NAC operation consists of several elements. Core of the network is a OpenFlow-compatible switch (Open vSwitch) and SDN controller (Ryu). Other vital components are: database server (MongoDB) and authentication server (HTTP  web server). Multiple PC hosts representing regular network users are connected to the core switch.

![Network topology](topo.png)
### Switch
The switch forwards traffic between authorized network users. Entries in flow forwarding table ensure that only authorized users are allowed to exchange traffic with other hosts.

### Controller
The application that runs on top of Ryu controller handles incoming data streams from yet-unknown devices. It communicates with database to verify network access privileges. In case of unauthorized device detected, it provides switches with specific flow entries that deny traffic to/from the device. In case of positive verification, traffic forwarding rules are installed in flow forwarding table.

### Database
Database holds information on devices and users. Basic parameters that allow to identify specific device (e.g. MAC address) are stored along with its status (e.g. authorized/unauthorized). Database chosen for the project is MongoDB.

### Management server
Management server is a simple web app that provides management panel for administrator user. The application allows to modify, remove and add device entries along with their status information.

### Host
The host is a regular end-device: desktop computer, laptop or a smartphone. Each host mus be authorized, before it can communicate with other hosts in the network.

## Operation scheme
This scenario describes steps performed by the NAC system upon detection of a new traffic stream from device.

1. Packet-In message is received at the controller. This means that traffic flow with unknown MAC address was originated from yet-unauthorized network device.
2. Controller application performs database lookup to check whether there is any information available on the new device. If the device is marked as authorized, go to step 4. Otherwise, mark device as unauthorized. Flow entries that deny further traffic from the device are installed to prevent exhaustion of controller's resources. These entries may optionally time out after specified time.
3. When an administrator changes status of the device (in this case: to authorized), mark the device as authorized in the database and notify controller application using REST API. \*
4. Controller application removes old entries related to the device and adds flow entries that allow forwarding traffic between new device and other hosts.
5. Device status may be updated by the administrator in the future. Proper actions will be taken, including database status update, controller API notification and update of flow table.
6. Voilà! The network connection works seamlessly. And it's a bit more secure than before.

\* The system may also optionally allow marking as unauthorized/removing device from the list. This would need another notification for controller to remove its flow entries.

## Environment
The network will be virtualized in Mininet. Startup scripts will ensure that proper amount of unique hosts will be generated. 

## Proactive flow entries
Proactive flow entries may be installed to prevent some of unauthorized devices from flooding controller with connection attempts. 

## Database contents

The database contains simple set of MAC addresses and their authorization status. Currently, two device states are considered. However, this may change in further development of the application.

### Device status

| Status | Description |
|--------|-------------|
|       0| UNAUTHORIZED|
|       1|   AUTHORIZED|

### Device entries

| MAC address | Status |
| ------------| -------|
| 4E:A8:7D:37:A9:C8|  0|
| FC:99:E9:94:8B:83|  1|
| BC:BF:62:F2:45:91|  0|

## Flow entries

Two types of flow entries are considered. 

* "allow" entry
* "deny" entry

Both entries have their cookie set to a value equal to the device's MAC address. It is feasible, as MAC address length is 48 bits and cookie field is 64 bits long. For example, device with address `FC:99:E9:94:8B:83` will have its related flow entries' cookie value set to `0xfc99e9948B83`. Both entries may timeout automatically, optimal values are to be considered.

### "allow" entry
This entry allows device to exchange traffic with other network devices by forwarding its packet just as regular L2 switch. It contains `NORMAL` action with `eth_src` match field equal to device's MAC address.

### "deny" entry
This entry denies device from exchanging traffic with other network devices by dropping its traffic. It contains `drop` (empty) action with `eth_src` match field equal to device's MAC address.

## References
1. Paul Göransson, Chuck Black, "Software Networks: A Comprehensive Approach", pp. 233-235 ("10.13 Access Control for the Campus")
2. Thomas D. Nadeau, Ken Gray, "SDN: Software Defined Networks", pp. 326-330 ("Network Access Control Replacement")

## Team
* Michał Rzepka
* Eryk Leniart
* Adam Niedziałkowski
* Maksymilian Chodacki