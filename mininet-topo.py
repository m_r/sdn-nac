from mininet.topo import Topo

__author__ = 'Michal Rzepka'


class NacTopo(Topo):

    def __init__(self):

        # Initialize topology
        Topo.__init__(self)

        # main switch
        s1 = self.addSwitch('s1')

        # regular user hosts
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')
        h3 = self.addHost('h3')
        h4 = self.addHost('h4')

        # server host
        hs = self.addHost('hs')

        # links
        self.addLink(h1, s1)
        self.addLink(h2, s1)
        self.addLink(h3, s1)
        self.addLink(h4, s1)

        self.addLink(hs, s1)

topos = {'nac': (lambda: NacTopo())}
