import requests
from flask import Flask, render_template, request, url_for
from pymongo import MongoClient
from werkzeug.utils import redirect

import db_config

app = Flask(__name__)

controller_api_host = '127.0.0.1'
controller_api_port = 8080
controller_api_status_endpoint = 'status/set'

STATUS_UNAUTHORIZED = 0
STATUS_AUTHORIZED = 1


def get_db_collection():
    # mongodb conf
    mongolab_uri = db_config.db_uri

    client = MongoClient(mongolab_uri, connectTimeoutMS=30000, socketTimeoutMS=None, socketKeepAlive=True)

    db = client.get_default_database()
    return db['users']


def db_add_or_update_device(mac_address, status):
    post = {mac_address: status}
    collection.update_one({}, {'$set': post}, upsert=True)

    status_update = {'mac': mac_address, 'status': status}
    update_controller(status_update)


def db_delete_device(mac_address):
    document = collection.find_one()
    post = {mac_address: document[mac_address]}
    collection.update_one({}, {'$unset': post}, {'multi': True})


def db_get_devices():

    document = collection.find_one(None, {'_id': 0})

    if document:
        return document
    else:
        return {}


def update_controller(payload):

    try:
        requests.post(
            'http://{}:{}/{}'.format(controller_api_host, controller_api_port, controller_api_status_endpoint),
            json=payload)
    except requests.exceptions.ConnectionError as e:
        print e
        pass


@app.route("/")
def get_devices():
    return render_template('panel.html', entries=db_get_devices())


@app.route('/add', methods=['POST'])
def add_device():
    db_add_or_update_device(request.form['mac'], int(request.form['status']))
    return redirect(url_for('get_devices'))


collection = get_db_collection()

if __name__ == "__main__":
    app.run()
