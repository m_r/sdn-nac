#!/usr/bin/env bash

CONTROLLER_IP=$1

if [ -z "$CONTROLLER_IP" ];
then
    echo "controller IP address required to run..."
    echo "usage: $0 <controller ip>"
    exit 1
fi

echo "running mininet, controller=$CONTROLLER_IP"
echo "sudo mn --custom mininet-topo.py --topo=nac --controller=remote,ip=$CONTROLLER_IP --switch=ovsk,protocols=OpenFlow13 --mac"

sudo mn --custom mininet-topo.py --topo=nac --controller=remote,ip="$CONTROLLER_IP" --switch=ovsk,protocols=OpenFlow13 --mac
