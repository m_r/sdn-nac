import BaseHTTPServer

PORT = 8000
REDIRECT_TARGET = 'http://www.google.pl'


class CaptivePortalRedirectHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def do_GET(self):

        # just trigger the logger
        print ''

        # send redirect response
        self.send_response(303)
        self.send_header("Location", REDIRECT_TARGET)
        self.send_header("Content-type", 'text/html')
        self.send_header("Content-Length", str(0))
        self.end_headers()


def serve(handler_class=CaptivePortalRedirectHandler, server_class=BaseHTTPServer.HTTPServer, protocol="HTTP/1.0"):

    # start server on specified port
    server_address = ('', PORT)

    handler_class.protocol_version = protocol
    httpd = server_class(server_address, handler_class)

    sa = httpd.socket.getsockname()

    print "Serving HTTP on", sa[0], "port", sa[1], "..."
    httpd.serve_forever()

if __name__ == '__main__':
    serve()
