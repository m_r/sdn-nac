#!/usr/bin/env python
import json
import logging

from pymongo import MongoClient
from ryu import utils
from ryu.app.wsgi import ControllerBase, route, WSGIApplication
from ryu.base import app_manager
from ryu.cmd import manager
from ryu.controller import ofp_event
from ryu.controller.dpset import DPSet
from ryu.controller.handler import MAIN_DISPATCHER, CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.lib import mac
from ryu.lib.packet import ethernet
from ryu.lib.packet import packet
from ryu.ofproto import ofproto_v1_3
from webob import Response

import db_config

logger = logging.getLogger('nac')
logger.setLevel(logging.INFO)

STATUS_UNAUTHORIZED = 0
STATUS_AUTHORIZED = 1

main_app_name = 'nac-app'

url_status_reset = '/status/set'


class NacApp(app_manager.RyuApp):

    _CONTEXTS = {
        'wsgi': WSGIApplication,
        'dpset': DPSet,
    }

    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(NacApp, self).__init__()

        wsgi = kwargs['wsgi']
        wsgi.register(NacAppRest, {main_app_name: self})

        self.dpset = kwargs['dpset']

    # mongodb conf
    mongolab_uri = db_config.db_uri

    client = MongoClient(mongolab_uri,
                         connectTimeoutMS=30000,
                         socketTimeoutMS=None,
                         socketKeepAlive=True)

    db = client.get_default_database()
    collection = db['users']

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def packet_in_handler(self, ev):

        """
        packet-in handler
        :param ev:
        """
        logger.info('packet-in event')

        msg = ev.msg
        dp = msg.datapath
        of_proto = dp.ofproto
        of_proto_parser = dp.ofproto_parser

        if msg.reason == of_proto.OFPR_NO_MATCH:
            reason = 'NO MATCH'
        elif msg.reason == of_proto.OFPR_ACTION:
            reason = 'ACTION'
        elif msg.reason == of_proto.OFPR_INVALID_TTL:
            reason = 'INVALID TTL'
        else:
            reason = 'unknown'

        self.logger.debug('OFPPacketIn received: '
                          'buffer_id=%x total_len=%d reason=%s '
                          'table_id=%d cookie=%d match=%s data=%s',
                          msg.buffer_id, msg.total_len, reason,
                          msg.table_id, msg.cookie, msg.match,
                          utils.hex_array(msg.data))

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        eth_dst = eth.dst
        eth_src = eth.src

        self.logger.info("packet-in %s->%s", eth_src, eth_dst)

        # check whether device is authorized
        if self.verify_device_status(eth_src):

            self.logger.info('traffic accepted: {}'.format(eth_src))

            self.remove_device_related_entries(dp, of_proto, of_proto_parser, eth_src)
            self.install_traffic_accept_entry(dp, of_proto, of_proto_parser, eth_src, msg.buffer_id)
        else:

            self.logger.info('traffic denied: {}'.format(eth_src))

            self.remove_device_related_entries(dp, of_proto, of_proto_parser, eth_src)
            self.install_traffic_deny_entry(dp, of_proto, of_proto_parser, eth_src, msg.buffer_id)

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):

        """
        switch-features handler
        :param ev:
        """
        msg = ev.msg
        dp = msg.datapath
        of_proto = dp.ofproto
        of_proto_parser = dp.ofproto_parser

        logger.info('switch-features event, dpid {}, clearing flow table'.format(dp.id))

        self.clear_flow_table(dp, of_proto, of_proto_parser)

    def verify_device_status(self, eth):

        """
        check whether device is authorized to send traffic across the network
        :param eth: device eth address
        :return:
        """
        document = self.collection.find_one()

        if not document:
            post = {eth: STATUS_UNAUTHORIZED}
            self.collection.update_one({}, {'$set': post}, upsert=True)
            return False

        if eth not in document.keys():

            logger.info('No MAC in db - inserting...')
            post = {eth: STATUS_UNAUTHORIZED}
            self.collection.update_one({}, {'$set': post}, upsert=True)

            return False
        else:
            logger.info('eth: ' + eth)

            if document[eth] == STATUS_AUTHORIZED:
                logger.info('User authorized')
                return True

            logger.info('User unauthorized')
            return False

    def install_traffic_accept_entry(self, dp, of_proto, of_proto_parser, eth, buffer_id=ofproto_v1_3.OFP_NO_BUFFER):

        """
        install flow entries that allow device to exchange traffic with other devices across the network
        :param dp: datapath
        :param of_proto: ofproto instance
        :param of_proto_parser: ofproto parser instance
        :param eth: device eth address
        :param buffer_id: packet-in buffer id
        """
        # flow match contains source MAC address that identifies the device
        match = of_proto_parser.OFPMatch(eth_src=eth)

        # flow action NORMAL causes the packets to be processed as regular L2 traffic
        flow_action = [of_proto_parser.OFPActionOutput(of_proto.OFPP_NORMAL, 0)]
        instruction = [of_proto_parser.OFPInstructionActions(of_proto.OFPIT_APPLY_ACTIONS,
                                                             flow_action)]

        # cookie will be equal to int value of mac address
        cookie = mac.haddr_to_int(eth)
        cookie_mask = 0

        # install the entry in first table
        table_id = 0

        # timeouts
        idle_timeout = 60
        hard_timeout = 0

        # priority
        priority = 1000

        req = of_proto_parser.OFPFlowMod(dp, cookie, cookie_mask,
                                         table_id, of_proto.OFPFC_ADD,
                                         idle_timeout, hard_timeout,
                                         priority, buffer_id,
                                         of_proto.OFPP_ANY, of_proto.OFPG_ANY,
                                         of_proto.OFPFF_SEND_FLOW_REM,
                                         match, instruction)
        dp.send_msg(req)

    def install_traffic_deny_entry(self, dp, of_proto, of_proto_parser, eth, buffer_id=ofproto_v1_3.OFP_NO_BUFFER):

        """
        install flow entries that denies device to exchange traffic with other devices across the network
        :param dp: datapath
        :param of_proto: ofproto instance
        :param of_proto_parser: ofproto parser instance
        :param eth: device eth address
        :param buffer_id: packet-in buffer id
        """
        # flow match contains source MAC address that identifies the device
        match = of_proto_parser.OFPMatch(eth_src=eth)

        # empty flow action packets to be DROPPED
        flow_action = []
        instruction = [of_proto_parser.OFPInstructionActions(of_proto.OFPIT_APPLY_ACTIONS,
                                                             flow_action)]

        # cookie will be equal to int value of mac address
        cookie = mac.haddr_to_int(eth)
        cookie_mask = 0

        # install the entry in first table
        table_id = 0

        # timeouts
        idle_timeout = 60
        hard_timeout = 0

        # priority
        priority = 1000

        req = of_proto_parser.OFPFlowMod(dp, cookie, cookie_mask,
                                         table_id, of_proto.OFPFC_ADD,
                                         idle_timeout, hard_timeout,
                                         priority, buffer_id,
                                         of_proto.OFPP_ANY, of_proto.OFPG_ANY,
                                         of_proto.OFPFF_SEND_FLOW_REM,
                                         match, instruction)
        dp.send_msg(req)

    def remove_device_related_entries(self, dp, of_proto, of_proto_parser, eth):
        """
        remove flow entries that apply to specified eth address of a device
        :param dp: datapath
        :param of_proto: ofproto instance
        :param of_proto_parser: ofproto parser instance
        :param eth: device eth address
        """
        # flow match contains source MAC address that identifies the device
        match = of_proto_parser.OFPMatch(eth_src=eth)

        # empty flow action packets to be DROPPED
        instruction = []

        # cookie will be equal to int value of mac address
        cookie = mac.haddr_to_int(eth)
        cookie_mask = 2**64 - 1

        # install the entry in first table
        table_id = 0

        # timeouts
        idle_timeout = 0
        hard_timeout = 0

        # priority
        priority = 1000

        # no buffer id
        buffer_id = of_proto.OFP_NO_BUFFER

        req = of_proto_parser.OFPFlowMod(dp, cookie, cookie_mask,
                                         table_id, of_proto.OFPFC_DELETE,
                                         idle_timeout, hard_timeout,
                                         priority, buffer_id,
                                         of_proto.OFPP_ANY, of_proto.OFPG_ANY,
                                         0,
                                         match, instruction)
        dp.send_msg(req)

    def clear_flow_table(self, dp, of_proto, of_proto_parser):

        """
        clear all entries in flow table
        :param dp: datapath
        :param of_proto: ofproto instance
        :param of_proto_parser: ofproto parser instance
        """

        # empty match
        match = of_proto_parser.OFPMatch()

        # empty instruction
        instruction = []

        # cookie ANY
        cookie = 0
        cookie_mask = 0

        # install the entry in first table
        table_id = of_proto.OFPTT_ALL

        # timeouts
        idle_timeout = 0
        hard_timeout = 0

        # priority
        priority = 0

        # no buffer id
        buffer_id = of_proto.OFP_NO_BUFFER

        req = of_proto_parser.OFPFlowMod(dp, cookie, cookie_mask,
                                         table_id, of_proto.OFPFC_DELETE,
                                         idle_timeout, hard_timeout,
                                         priority, buffer_id,
                                         of_proto.OFPP_ANY, of_proto.OFPG_ANY,
                                         0,
                                         match, instruction)
        dp.send_msg(req)


class NacAppRest(ControllerBase):

    def __init__(self, req, link, data, **kwargs):

        super(NacAppRest, self).__init__(req, link, data, **kwargs)
        self.nac_app = data[main_app_name]

    @route(main_app_name, url_status_reset, methods=['POST'])
    def reset_rules(self, req, **kwargs):

        # parse json
        try:
            req_json = json.loads(req.body)
        except ValueError:

            response_msg = {'error': 'invalid json request'}
            return Response(status=400, json_body=response_msg)

        # extract parameters
        try:
            mac_str = req_json['mac']
            status = req_json['status']
        except KeyError:

            response_msg = {'error': 'invalid json request structure'}
            return Response(status=400, json_body=response_msg)

        # validate mac
        try:
            mac.haddr_to_int(mac_str)
        except ValueError:

            response_msg = {'error': 'invalid mac format'}
            return Response(status=400, json_body=response_msg)

        # remove traffic entries on all available switches
        for d in self.nac_app.dpset.get_all():

            dpid, datapath = d
            of_proto = datapath.ofproto
            of_proto_parser = datapath.ofproto_parser

            logger.info('resetting forwarding rules for {} on dpid {}'.format(mac_str, dpid))

            self.nac_app.remove_device_related_entries(datapath, of_proto, of_proto_parser, mac_str)

        # OK response
        response_msg = {'msg': 'mac {}, status {}, forwarding rules reset'.format(mac_str, status)}
        return Response(status=200, json_body=response_msg)

if __name__ == '__main__':
    manager.main()
